from flask import current_app, Flask, request, abort
import random
import string

rand = random.SystemRandom()
app = Flask(__name__)


def filter_letters(letters, option):
    option = str(option).lower()
    if option in ['no', 'false', '0']:
        return ''
    if option in ['', 'yes', 'true', '1']:
        return letters

    # option is letters not boolean
    return ''.join([c for c in letters if c in option])


@app.route('/')
def generate():
    upper_option = request.args.get('upper', True)
    lower_option = request.args.get('lower', True)
    symbol_option = request.args.get('symbol', True)
    number_option = request.args.get('number', True)
    length = request.args.get('len', 32)

    try:
        length = int(length)
    except ValueError:
        abort(400)

    letters = \
        filter_letters(string.ascii_uppercase, upper_option) + \
        filter_letters(string.ascii_lowercase, lower_option) + \
        filter_letters('!#$%^&*(){}~`|_?+-;:.,@<>', symbol_option) + \
        filter_letters(string.digits, number_option)

    if len(letters) == 0:
        abort(400)

    text = ''.join([rand.choice(letters) for i in range(length)])
    return current_app.response_class(text, mimetype='text/plain')


if __name__ == '__main__':
    import os
    app.run(debug=os.environ.get('DEBUG', False))
